  
module.exports = {
    "port": 8080,
    "ipconfig" : "127.0.0.1",
    "mongoosePort" : 27017,
    "mongooseCollection" : "games",
    "appEndpoint": "http://localhost:8080",
    "apiEndpoint": "http://localhost:8080",    
};