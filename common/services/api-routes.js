// Filename: api-routes.js
// Initialize express router
let router = require('express').Router();

// Add games routes:
const GamesRouter = require('../../games/routes.config');
GamesRouter.routesConfig(router);

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to RESTHub crafted with love!'
    });
});

// Export API routes
module.exports = router;