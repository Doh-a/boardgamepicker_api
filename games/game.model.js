// gamesModel.js
const mongoose = require('../common/services/mongoose.service').mongoose;
const Schema = mongoose.Schema;

// Setup schema
var gameSchema = Schema({
    name: {
        type: String,
        required: true
    },
    minPlayers: {
        type: Number,
        required: true
    },
    maxPlayers: {
        type: Number,
        required: false
    },
    configuration : {
        type:String, // competition, coop, mix
        required : false
    },
    style : {
        type:String, // party game, strategy, dice
        required : false
    },
    level:{
        type:String, // beginner, medium, expert
        required : false
    },
    duration:{
        type:Number,
        required : false
    },
    create_date: {
        type: Date,
        default: Date.now
    },
    notes: {
        type:String,
        required: false
    },
    playedCounter: {
        type:Number,
        required : true,
        default : 0
    }
});

// Export Game model
var Game = module.exports = mongoose.model('game', gameSchema);