// GameController.js
// Import Game model
const Game = require('./game.model');

// List all the games
exports.list = function (req, res) {

    let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 1000;
    let orderCriteria = req.query.orderCriteria ? req.query.orderCriteria : "name";
    let orderOrder = "none";
    switch(orderCriteria)
    {
        case "name" :
            orderOrder = 1;
            break;
        case "playedCounter" :
            orderOrder = -1;
            break;
        default:
            res.status(500).send("non valid order criteria, only name and playedCounter are supported");
            return;            
    }

    let page = 0;

    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }

    listGames(limit, page, orderCriteria, orderOrder)
        .then((result) => {
            res.status(200).send(result);
    })
};

// Insert a game:
exports.insert = (req, res) => {
    createGame(req.body)
        .then((result) => {
            res.status(201).send({id: result._id});
        }, (err) => {
            res.status(500).send(err)
        });        
};

exports.getRandom = (req, res)  => {
    getRandomGame(req.body)
        .then((result) => {
            res.status(201).send(result);
        }, (err) => {
            res.status(500).send(err)
        });       
}

exports.getById = (req, res)  => {
    if(req.params.gameId !== undefined)
        getGameById(req.params.gameId)
            .then((result) => {
                res.status(201).send(result);
            }, (err) => {
                res.status(500).send("Could not find game")
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.getByName = (req, res)  => {
    if(req.params.gameName !== undefined)
        getGamesByName(req.params.gameName)
            .then((result) => {
                res.status(201).send(result);
            }, (err) => {
                res.status(500).send("Could not find game")
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.getByStyle = (req, res)  => {
    if(req.params.gameStyle !== undefined)
        getGamesByStyle(req.params.gameStyle)
            .then((result) => {
                res.status(201).send(result);
            }, (err) => {
                res.status(500).send("Could not find game matching style " + req.params.gameStyle);
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.getByConfig = (req, res)  => {
    if(req.params.gameConfig !== undefined)
        getGamesByConfig(req.params.gameConfig)
            .then((result) => {
                res.status(201).send(result);
            }, (err) => {
                res.status(500).send("Could not find game matching configuration " + req.params.gameStyle);
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.findGameSuggestionMultiCriteria = (req, res) => {
    if(req.body != "" && req.body !== undefined)
    {
        console.log(req.body);
        let filter = {};

        for (const key of Object.keys(req.body))
        {
            switch(key)
            {
                case "playersCount":
                    if(req.body.maxDuration > 0)
                    {                
                        filter.minPlayers = { $lte : req.body.playersCount};

                        // This should be improved, as this would not work if we have a second $or.
                        filter["$or"] = [{maxPlayers : { $gte : req.body.playersCount }}, {maxPlayers : {$exists:false}}];
                    }
                    break;
                
    
                case "style":
                    if(req.body.style != '')                              
                        filter.style = req.body.style;
                    break;
    
                case "configuration":
                    if(req.body.configuration != '')
                        filter.configuration = req.body.configuration;
                    break;
    
                case "level":  
                    if(req.body.level != '')              
                    {
                        if(req.body.level == "beginner")
                            filter.level = req.body.level;
                        else
                        {
                            if(req.body.level == "medium")
                                filter.level = ["beginner", "medium"];
                            else 
                                if(req.body.level != "expert")
                                {
                                    res.status(500).send("The only levels available are beginner, medium and expert");
                                    return;
                                }
                        }
                    }
                    break;
    
                case "maxDuration":
                    if(req.body.maxDuration > 0)
                        filter.duration = { $lte : req.body.maxDuration };
                    break;

                default:
                    res.status(500).send(key + " is not a valid filter, you can only use playersCount, style, configuration, level and maxDuration.");
                    return;
            }
        }

        Game.aggregate([ {$match: filter} ])
        .exec(function (err, games) {
            if (err) {
                res.status(500).send(err);
            } else {
                if(games.length == 0)
                {
                    res.status(500).send("No games found in this criteria, we should buy more games!.");                
                    return;
                }
                else
                {
                    let initialTotalPlayedCounter = 0;
                    for(let game of games)
                    {                    
                        initialTotalPlayedCounter += parseInt(game.playedCounter)+1;
                    }                

                    // Revert (to have more wieght on the less played games)
                    let totalPlayedCounter = 0;
                    for(let game of games)
                    {     
                        totalPlayedCounter += (initialTotalPlayedCounter - parseInt(game.playedCounter) - 1);
                    }

                    // This weighted random selection is not great, but that's the best I could find.
                    // Here is the source: http://peterkellyonline.blogspot.com/2012/02/weighted-random-selection-in-php.html

                    // choose a random between 1 and the sum of the weights.
                    let randomValue = Math.floor(Math.random() * (totalPlayedCounter)); 
                    for(let game of games)
                    { 
                        // ***The next two lines are the heart of this algorithm***
                        // decrement the random by the current weighting.
                        randomValue -= (initialTotalPlayedCounter - parseInt(game.playedCounter) - 1);        
                        // The larger the weighting, the more likely random is less than zero.
                        if(randomValue <= 0) {
                            res.status(201).send(game)
                            break;  
                        }
                    } 
                }                
            }
        });
    }
    else
    {
        res.status(500).send("You need to pass you filters in the body as a json.");
    }
}

exports.incrementGamePlayedCounter = (req, res) => {
    if(req.params.gameId !== undefined)
        getGameById(req.params.gameId)
            .then((result) => {
                result.playedCounter++;                
                result.save();
                res.status(200).send(result.playedCounter.toString());
            }, (err) => {
                res.status(500).send("Could not find game")
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.decrementGamePlayedCounter = (req, res) => {
    if(req.params.gameId !== undefined)
        getGameById(req.params.gameId)
            .then((result) => {
                result.playedCounter--;                
                result.save();
                res.status(200).send(result.playedCounter.toString());
            }, (err) => {
                res.status(500).send("Could not find game")
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.patchById = (req, res)  => {
    if(req.params.gameId !== undefined)
        getGameById(req.params.gameId)
            .then((game) => {
                
                // Doing this allows to compare dynamically to the schame, so if we update the schema we will automatically manage the new fields in the update
                for (const key of Object.keys(req.body))
                {
                    if(key in Game.schema.obj)
                    {                       
                        game[key] = req.body[key];
                    }
                    else
                    {
                        res.status(500).send(key + " is not a valid field in the schema of a Game.")
                        return;
                    }
                }
                
                game.save().then((result, err) => {
                    if (err) {
                        reject(err);
                    } else {
                        res.status(200).send(result);
                    }
                })
                
            }, (err) => {
                res.status(500).send("Could not find game matching configuration " + req.params.gameStyle + "\n" + err);
            });  
    else
        res.status(500).send("You need to provide an id for the game.")
}

exports.removeById = (req, res)  => {
    if(req.params.gameId !== undefined)
        Game.deleteOne({_id : req.params.gameId}).then((result, err) => {
            if (err) {
                reject(err);
            } else {
                res.status(200).send(result);
            }
        });
}

getGame = function (callback, limit) {
    Game.find(callback).limit(limit);
}

getGameById = (gameId) => {
    return new Promise((resolve, reject) => {Game.findById(gameId)
        .exec(function (err, game) {
            if (err) {
                reject(err);
            } else {
                resolve(game);
            }
        });
    });        
};

getGamesByName = (gameName) => {
    return new Promise((resolve, reject) => {Game.find({"name" : new RegExp(gameName, 'i')})
        .exec(function (err, game) {
            if (err) {
                reject(err);
            } else {
                resolve(game);
            }
        });
    });        
};

getGamesByStyle = (gameStyle) => {
    return new Promise((resolve, reject) => {Game.find({"style" : new RegExp(gameStyle, 'i')})
        .exec(function (err, game) {
            if (err) {
                reject(err);
            } else {
                resolve(game);
            }
        });
    });        
};

getGamesByConfig = (gameConfig) => {
    return new Promise((resolve, reject) => {Game.find({"configuration" : new RegExp(gameConfig, 'i')})
        .exec(function (err, game) {
            if (err) {
                reject(err);
            } else {
                resolve(game);
            }
        });
    });        
};

listGames = (perPage, page, orderCriteria, orderOrder) => {
    return new Promise((resolve, reject) => {
        Game.find()
            .sort([[orderCriteria, orderOrder]])
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, games) {
                if (err) {
                    reject(err);
                } else {
                    resolve(games);
                }
            })
    });
};

createGame = (gameData) => {
    const game = new Game(gameData);
    return game.save();
};

getRandomGame = () => {
    return new Promise((resolve, reject) => {
        Game.aggregate([{ $sample : {size: 1}}])
        .exec(function (err, games) {
            if (err) {
                reject(err);
            } else {
                resolve(games);
            }
        })
    ;});
};
