const GameController = require('./game.controller');
const config = require('../common/config/env.config');

exports.routesConfig = function (app) {
    app.get('/games', [
        GameController.list
    ]);
    app.post('/insertGame', [     
        GameController.insert
    ]);
    app.get('/game', [        
        GameController.getRandom
    ]); 
    app.get('/gameById/:gameId', [        
        GameController.getById
    ]);   
    app.get('/gamesByName/:gameName', [        
        GameController.getByName
    ]);
    app.post('/game', [        
        GameController.findGameSuggestionMultiCriteria
    ]);
    app.get('/gamePlayed/:gameId', [        
        GameController.incrementGamePlayedCounter
    ]);
    app.get('/gameCancelPlayed/:gameId', [        
        GameController.decrementGamePlayedCounter
    ]);
    app.get('/gamesByStyle/:gameStyle', [        
        GameController.getByStyle
    ]);
    app.get('/gamesByConfiguration/:gameConfig', [        
        GameController.getByConfig
    ]);
    app.patch('/game/:gameId', [
        GameController.patchById
    ]);                   
    app.delete('/game/:gameId', [
        GameController.removeById
    ]);
};