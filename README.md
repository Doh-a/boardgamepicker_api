# boardgamepicker_api

## Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Contributing](../CONTRIBUTING.md)

## About <a name = "about"></a>

Board Game Picker API, is a restful api allowing to access the list of board games I own with the capacity to randomly pick one game depending on multiple criterai.
The main goal is to stop playing always the same games and forgetting awesome game on the bottom shelf.

## Getting Started <a name = "getting_started"></a>

Not sure yet.

### Prerequisites

You'll need node & npm, then run npm install.
You'll need a mongoserver as well.

### Installing

not sure yet

## Usage <a name = "usage"></a>

start node index (nodemon index if you want to restart on the fly), then open http://localhost:8080
