// FileName: index.js
// Import express
let express = require('express')
// Initialize the app
let app = express();

var cors = require('cors');
// use it before all route definitions
app.use(cors({origin: '*'}));

let config = require("./common/config/env.config");

// Setup server port
var port = process.env.PORT || config.port;

let bodyParser = require('body-parser');
app.use(bodyParser.json());

// Import routes
let apiRoutes = require("./common/services/api-routes")

// Use Api routes in the App
app.use(apiRoutes)

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Launch app to listen to specified port
app.listen(port, config.ipconfig, function () {
     console.log("Running RestHub on port " + port);
});